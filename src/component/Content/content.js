import { Grid } from "@mui/material"
import PopularCourses from "./PopularCourses"
import RecommendedCourses from "./RecommendedCourses"
import Slide from "./Slide"
import TrendingCourses from "./TrendingCourses"


function Content() {
    return (
        <Grid>
            <Slide></Slide>
            <RecommendedCourses></RecommendedCourses>
            <PopularCourses></PopularCourses>
            <TrendingCourses></TrendingCourses>
        </Grid>
    )
}


export default Content
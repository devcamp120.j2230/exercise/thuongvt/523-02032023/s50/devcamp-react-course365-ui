import { Card, CardBody, CardFooter, CardText, CardTitle } from "reactstrap"
import BookmarkIcon from '@mui/icons-material/Bookmark';
import WatchLaterIcon from '@mui/icons-material/WatchLater';
import { CardContent, Grid, Typography } from "@mui/material";


function CourseCard(props) {
    const courseName = props.courseName;
    const coverImage = props.coverImage;
    const duration = props.duration;
    const level = props.level;
    const discountPrice = props.discountPrice;
    const price = props.price;
    const teacherName = props.teacherName;
    const teacherPhoto = props.teacherPhoto;


    return (
        <Card style={{ width: '18rem', marginLeft: "10px", marginRight: "10px", marginTop: "0.5%", marginBottom: "2%"}}>
            {/* <CardContent component="img" style={{height:"11rem",width:"18rem"}} src={require("../../assetment/"+coverImage)}>
           </CardContent> */}
           <Typography component="img" style={{height:"11rem",width:"18rem"}} src={require("../../assetment/"+coverImage)}>
            
           </Typography>
            <CardBody style={{marginLeft:"10px"}}>
                <CardTitle style={{marginTop:"5px",minHeight:"45px"}}>
                {courseName}
                </CardTitle>
                <CardText>
                    <p><WatchLaterIcon fontSize="small"></WatchLaterIcon>{duration}</p>
                    <h4>{level}</h4>
                </CardText>
            </CardBody>
            <hr></hr>
            <CardFooter style={{fontSize:"medium", display:"flex", justifyContent:"center"}}>
            <Typography
                                component="img"
                                item
                                src={require("../../assetment/"+teacherPhoto)}
                                sx={{
                                    marginBottom:"5px",
                                    marginRight:"10px",
                                    width: "40px",
                                    borderRadius: "50%",
                                }}
                            />
                {/* <Grid item xs={2}>
                <img src={""}  alt="logo" style={{width:"40px", borderRadius:"50%"}}></img>
                </Grid> */}
                <Grid item xs={6} mt={1}>
                <h>{teacherName}</h>
                </Grid>
                <Grid item xs={2} mt={1}>
                <BookmarkIcon></BookmarkIcon>
                </Grid>
            </CardFooter>
        </Card>
    )
}

export default CourseCard
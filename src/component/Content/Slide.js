import { Grid } from "@mui/material"
import Button from '@mui/material/Button';
import imgpic from "../../assetment/images/ionic-207965.png"

function Slide() {
    return (
        <Grid container style={{ backgroundColor: "#0066FF", height: "600px" }}>
            <Grid item xs={4} textAlign="center" style={{ marginTop: "10%", marginBottom: "10%", marginLeft: "5%", marginRight: "5%", color: "white" }} >
                <h3>Welcome to Ionic Course365 Learning Center</h3>
                <p>Ionic Course365 is an online learning and teaching marketplace with over 5000 courses and 1 million students. Instructor and expertly crafted courses, designed for the modern students and entrepreneur.
                </p>
                <Grid item>
                <Button variant="contained" style={{margin:"10px", minWidth:"200px"}} color="warning" >Brow Course</Button>
                <Button variant="contained" style={{margin:"10px",minWidth:"200px"}} color="warning">Become an Intrucst</Button>
                </Grid>
            </Grid>
            <Grid item xs={4} >
                <img src={imgpic} style={{ backgroundPosition: "center", backgroundSize: "contain", backgroundRepeat: "no-repeat", marginLeft: "200px", maxHeight: "550px", marginTop: "30px" }}></img>
            </Grid>
        </Grid>
    )
}
export default Slide
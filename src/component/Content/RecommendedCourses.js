import { Grid } from "@mui/material"
import axios from "axios";
import { useEffect, useState } from "react"
import CourseCard from "./CourseCard"

function RecommendedCourses() {
    const [courses, setCourses] = useState([])

    const axiosCall = async (url, body) => {
        const response = await axios(url, body);
        console.log(response.data)
        return response.data;
    };

    const getAllHandle = () => {
        axiosCall("https://630890e4722029d9ddd245bc.mockapi.io/api/v1/courses")
            .then(result => saveDataToCoursePopular(result))
            .catch(error => console.log('error', error.message));
    };


    const saveDataToCoursePopular = (data) => {
        const popularData = data.filter(val => {
            return val.isPopular;
        })
        console.log(popularData);
        setCourses(popularData);
    };

    useEffect(() => {
        getAllHandle()
    }, []);

    return (
        <>
            <h2 style={{ marginLeft: "5%" }}>Recommneded</h2>
            <Grid style={{ display: "flex", flexWrap: "wrap", justifyContent: "center" }}>
                {courses.slice(0, 4).map((val, i) => {
                    return <CourseCard
                        key={i}
                        courseName={val.courseName}
                        coverImage={val.coverImage}
                        duration={val.duration}
                        level={val.level}
                        discountPrice={val.discountPrice}
                        price={val.price}
                        teacherName={val.teacherName}
                        teacherPhoto={val.teacherPhoto}
                    ></CourseCard>
                })}

            </Grid>
        </>
    )
}
export default RecommendedCourses
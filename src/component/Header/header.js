import { Grid, Typography } from "@mui/material"
import { Button, Input } from "reactstrap"
import logo from "../../assetment/images/Ionic_logo.png"
function Header() {
    return (
        <Grid container mt={3} style={{ display: "flex", justifyContent: "space-between"}}>
            <Grid item xs={2} md={2} ld={2} sm={12} textAlign="center" mt={2}>
                <a href="#"><img src={logo}></img></a>
            </Grid>
            <Grid item style={{ display: "flex", justifyContent: "center" }} >
                <Typography style={{ margin: "20px" }}>
                    <a href="#">Home</a>
                </Typography>
                <Typography style={{ margin: "20px" }}>
                    <a href="#">Brow Course</a>
                </Typography>
                <Typography style={{ margin: "20px" }}>
                    <a href="#">About</a>
                </Typography>
            </Grid>
            <Grid xs={4}>

            </Grid>
            <Grid item style={{ display: "flex", justifyContent: "center", margin: "20px" }} textAlign="center">
                <Input placeholder="Nhập tìm kiếm" style={{ marginRight: "30px" }}></Input>
                <Button color="primary">Tìm kiếm</Button>
            </Grid>
        </Grid>


    )
}

export default Header
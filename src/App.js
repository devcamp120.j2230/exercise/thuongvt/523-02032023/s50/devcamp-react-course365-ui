
import { Grid } from '@mui/material';
import './App.css';
import Content from './component/Content/content';
import Footer from './component/Footer/footer';
import Header from './component/Header/header';

function App() {
  return (
    <div>
        <Grid container>
          <Header></Header>
          <Content></Content>
          <Footer></Footer>
        </Grid>
    </div>
  );
}

export default App;
